# Use-Case Raumklima-Monitoring

## Projektbeschreibung

In diesem Use-Case wurde die Integration des "ELSYS ERS CO2 Lite" durchgeführt. Der Sensor misst die relative Luftfeuchtigkeit, Temperatur und CO2-Konzentration in einem Raum.

Der Use-Case wurde auf der FIWARE-basierten [Urban Dataspace Platform](https://gitlab.com/urban-dataspace-platform/core-platform) entwickelt und integriert.

## Installationsanleitung

Um den Anwendungsfall zu installieren sind folgende Schritte nötig:  
1. Eine lokale Maschine für die Installation ist vorzubereiten (Siehe https://gitlab.com/urban-dataspace-platform/core-platform/-/blob/master/00_documents/Admin-guides/cluster.md).
2. Eine NodeRED-Instanz deployen oder eine vorhandene Instanz verwenden:
    - Deployment analog zu den *demo_usecases*: https://gitlab.com/urban-dataspace-platform/use_cases/demo_usecases/deployment
3. *flow.json* in der NodeRED-Instanz deployen.

## Gebrauchsanweisung

Der NodeRED-Flow ruft die Daten per HTTP-Request von der ElementIOT-API ab. Dabei kann eine Folder-ID angegeben werden, um alle Devices aus einem Ordner mit einem Request abzufragen. Für die ElementIOT-API ist ein API-Key erforderlich.

Diese Daten werden in das NGSI-kompatible [AirQualityObserved Smart-Data-Model](https://github.com/smart-data-models/dataModel.Environment/blob/master/AirQualityObserved/README.md) umgewandelt und an die Datenplattform gesendet:

```json
{
   "id":"urn:ngsi-ld:AirQualityObserved:01650f58-0d5d-430c-b5a2-4a7765ec3a02",
   "type":"AirQualityObserved",
   "name":{
      "type":"Text",
      "value":"RHI R216 56B1D"
   },
   "alternateName":{
      "type":"Text",
      "value":"ers-co2-lite-56b1d"
   },
   "dateObserved":{
      "type":"DateTime",
      "value":"2024-05-03T13:40:23.130Z"
   },
   "co2":{
      "type":"Number",
      "value":478
   },
   "relativeHumidity":{
      "type":"Number",
      "value":45
   },
   "temperature":{
      "type":"Number",
      "value":22.6
   }
}
```

## Kontaktinformationen

Dieses Projekt wurde von der [Hypertegrity AG](https://www.hypertegrity.de/) entwickelt.

## Lizenz

Dieses Projekt wird unter der EUPL 1.2 veröffentlicht.

## Link zum Original-Repository

https://gitlab.com/urban-dataspace-platform/use_cases/integration/raumklima_monitoring/nr-elsys-co2-lite